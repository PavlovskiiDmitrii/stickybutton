import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './StickyButton.scss'

//TODO: refactoring all component
const StickyButton = ({ diameterBall, funcOnClick, demo, textButton }) => {

    const [y, setY] = useState(0);
    const [x, setX] = useState(0);
    const [mouse, setMouse] = useState(null);
   
    const [transition, setTransition] = useState(0.2);
    const [move, setMove] = useState(null);

    const elem = useRef(null);

    useEffect(() => {
        if (move === true) {
            const id = setInterval(() => {
                let elemProps = elem.current.getBoundingClientRect();
                setX((window.mouser.x - elemProps.x - elemProps.width/2)/2);
                setY((window.mouser.y - elemProps.y - elemProps.height/2)/2);
            }, 20);
            return () => clearInterval(id);
        }
    },[move]);

    useEffect(() => {
        if (mouse !== null) {
            window.mouser = mouse
        }
        if (mouse === null) {
            window.mouser = null;
            setTransition(0.2)
        }
    },[mouse]);

    const onMouseMove = (e) => {
        setMouse({x:e.clientX, y:e.clientY });
    }

    const onMouseLeave = () => {
        setMove(null);
        setMouse(null);
        setTransition(0.2);
        setY(0);
        setX(0);
    }

    const onMouseEnter = () => {
        setMove(true);
        setTimeout(() => {
            if (window.mouser) setTransition(.15);
        }, 100);
        setTimeout(() => {
            if (window.mouser) setTransition(.1);
        }, 200);
        setTimeout(() => {
            if (window.mouser) setTransition(0);
        }, 300);
    }

    const COF_ROTATE = diameterBall/100 * 1.2;
    const COF_BACKGROUND = diameterBall/2;

    return ( 
        <div ref={elem} className={`stickyButton__wrap ${demo ? 'stickyButton__wrap_demo' : ''}`}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            onMouseMove={onMouseMove}
            >
            <div className="stickyButton"
                style={{
                    width: diameterBall,
                    height: diameterBall,
                    transform: `translateX(${x}px) translateY(${y}px)`,
                    transition: `${transition}s`,
                    boxShadow: `rgba(0 0 0 / 0.4) ${-x/4}px ${-y/4}px 50px -12px`,
                    background: `radial-gradient(circle at ${COF_BACKGROUND + x/2}px ${COF_BACKGROUND + y/2}px,  rgb(255 255 255/ 0.8), rgb(0 0 0/ 0.2))`
                    }}>
                        <div className="stickyButton__text" style={{
                            width: `${diameterBall/2}px`,
                            height: `${diameterBall/2}px`,
                            fontSize: `${diameterBall/200}rem`,
                            transform: `translateX(${x/2}px) 
                                        translateY(${y/2}px) 
                                        rotateY(${-x/COF_ROTATE}deg) 
                                        rotateX(${y/COF_ROTATE}deg)`,
                        }}
                        onClick={funcOnClick}
                        >
                            {textButton}
                        </div>
            </div>
        </div>
     );
}

StickyButton.propTypes = {
    diameterBall: PropTypes.number,
    funcOnClick: PropTypes.func,
    demo: PropTypes.bool,
    textButton: PropTypes.string
};

StickyButton.defaultProps = {
    diameterBall: 200,
    funcOnClick: () => {alert('Привет')},
    demo: false,
    textButton: 'Click me'
};
 
export default StickyButton;