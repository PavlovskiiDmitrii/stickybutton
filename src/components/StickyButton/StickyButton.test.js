import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import StickyButton from './StickyButton';

configure({adapter: new Adapter()});

const setUp = (props) => shallow(<StickyButton {...props}/>);

describe('<StickyButton />', () => {
  it('should render component with demo', () => {
    const component = setUp({'demo' : true});
    const wrapper = component.find(".stickyButton__wrap_demo");
    expect(wrapper.length).toBe(1);
  });

  it('should render component without demo', () => {
    const component = setUp({'demo' : false});
    const wrapper = component.find(".stickyButton__wrap_demo");
    expect(wrapper.length).toBe(0);
  });

  it('should render component with props', () => {
    const component = shallow(<StickyButton textButton={'tttt'}/>);
    expect(component).toMatchSnapshot();
  });

  it('should render component without props', () => {
    const component = shallow(<StickyButton/>);
    expect(component).toMatchSnapshot();
  });
});